ARG ALPINE_VERSION
ARG PYTHON_VERSION
ARG JEDILS_VERSION

FROM python:${PYTHON_VERSION}-alpine${ALPINE_VERSION} AS builder

ARG JEDILS_VERSION

RUN python3 -m pip install -U --no-cache-dir \
        pip \
        setuptools \
        wheel && \
    mkdir -p /wheelhouse && \
    python3 -m pip wheel -w /wheelhouse --no-cache-dir \
        jedi-language-server==${JEDILS_VERSION}


FROM python:${PYTHON_VERSION}-alpine${ALPINE_VERSION}

ARG JEDILS_VERSION

COPY --from=builder /wheelhouse /wheelhouse
RUN apk add --no-cache \
        nmap-ncat && \
    python3 -m pip install -U --no-cache-dir \
        pip \
        setuptools && \
    python3 -m pip install --no-index --find-links=/wheelhouse --no-cache-dir \
        jedi-language-server==${JEDILS_VERSION}

CMD ["jedi-language-server"]
